public class CoolMoveBehaviour implements MoveBehaviour {

    @Override
    public MoveType move() {
        return MoveType.COOPERATE;
    }
}
