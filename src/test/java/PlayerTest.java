import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

public class PlayerTest {

   @Test
    public void shouldNotMakeMove() {
        Player player = new Player(new ConsoleMoveBehaviour(new Scanner("0")));
        MoveType move = player.move();
        Assert.assertNotEquals(move, MoveType.COOPERATE);
    }

    @Test
    public void shouldMakeMoveWhenCooperate() {
        Player player = new Player(new ConsoleMoveBehaviour(new Scanner("1")));
        MoveType move = player.move();
        Assert.assertEquals(move, MoveType.COOPERATE);
    }

    @Test
    public void shoudMakeMoveWhenCheat() {
        Player player = new Player(new ConsoleMoveBehaviour(new Scanner("2")));
        MoveType move = player.move();
        Assert.assertEquals(move, MoveType.CHEAT);
    }
    @Test
    public void shoudMakeMoveOnCoolType() {
        Player player = new Player(new CoolMoveBehaviour());
        MoveType move = player.move();
        Assert.assertEquals(move, MoveType.COOPERATE);
    }

    @Test
    public void shouldFailOnMakeMoveOnCoolType() {
        Player player = new Player(new CoolMoveBehaviour());
        MoveType move = player.move();
        Assert.assertNotEquals(move, MoveType.CHEAT);
    }
}
